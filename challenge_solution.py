#!/usr/bin/python

#####################################
# TECHNOCAMPS
# CODING CHALLENGE
#####################################
# YOUR CHALLENGE IS SIMPLE:
#
# Build an application that drops a
# trail of a type of block behind a
# player as they move!
#####################################
# YOUR APPLICATION MUST:
#####################################
# 1) Ask the user for the ID of the
# block they wish to follow them
# 2) The blocks must follow behind
# the player, no skipping ahead!
# 3) If the player selects TNT
# (ID: 46) the block must also be
# armed, with a 1 second delay to
# explosion.
# 4) Your application must prevent
# all error conditions, for example,
# not entering an ID for a block.
######################################

# Do some imports
import mcpi.minecraft as minecraft
import time

# Instantiate the minecraft object
mc = minecraft.Minecraft.create("localhost")

# Now we ask for the block
block = raw_input("Please enter the ID of the block: ")

# Now some checking, if it is empty, we use dirt
if block == "":
    block = 3
else:
    block = block
    
# Now, we find out if they used TNT?
if block == 46:
    tnt = True
else:
    tnt = False

# Now we can start the loop!
while true:
    # Where is the player?
    pos = mc.player.getTilePos()
    # And now we subtract 1 from behind them?
    time.sleep(0.1)
    blockBehind = mc.getBlock(pos.x, pos.y, pos.z)
    # Is the block air or water?
    if blockBehind != 0 and blockBehind != 9:
        if tnt:
            mc.setBlock(pos.x, pos.y, pos.z, block, 1)
        else:
            mc.setBlock(pos.x, pos.y, pos.z, block)
